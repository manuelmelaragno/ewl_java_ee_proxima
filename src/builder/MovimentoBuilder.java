package builder;

import dto.ContoCorrenteDTO;
import dto.MovimentoDTO;
import model.ContoCorrente;
import model.Movimento;

public class MovimentoBuilder {

	public static Movimento convert(MovimentoDTO dtoMovimento) {
		Movimento movimento = new Movimento();

		if (dtoMovimento.getIdMovimento() != null) {
			movimento.setIdMovimento(dtoMovimento.getIdMovimento());
		}
		if (dtoMovimento.getDataMovimento() != null) {
			movimento.setDataMovimento(dtoMovimento.getDataMovimento());
		}

		if (dtoMovimento.getContoCorrente() != null) {

			ContoCorrente conto = ContoCorrenteBuilder.convert(dtoMovimento.getContoCorrente());

			movimento.setContoCorrente(conto);
		}

		if (dtoMovimento.getImporto() != null) {
			movimento.setImporto(dtoMovimento.getImporto());
		}

		if (dtoMovimento.getTipoMovimento() != null && !dtoMovimento.getTipoMovimento().trim().equals("")) {
			movimento.setTipoMovimento(dtoMovimento.getTipoMovimento());
		}

		return movimento;
	}

	public static MovimentoDTO convert(Movimento movimento) {
		MovimentoDTO dtoMovimento = new MovimentoDTO();

		if (movimento.getIdMovimento() != null) {
			dtoMovimento.setIdMovimento(movimento.getIdMovimento());
		}

		if (movimento.getDataMovimento() != null) {
			dtoMovimento.setDataMovimento(movimento.getDataMovimento());
		}

		if (movimento.getContoCorrente() != null) {
			ContoCorrenteDTO dtoConto = ContoCorrenteBuilder.convert(movimento.getContoCorrente());

			dtoMovimento.setContoCorrente(dtoConto);
		}

		if (movimento.getImporto() != null) {
			dtoMovimento.setImporto(movimento.getImporto());
		}

		if (movimento.getTipoMovimento() != null && !movimento.getTipoMovimento().trim().equals("")) {
			dtoMovimento.setTipoMovimento(movimento.getTipoMovimento());
		}

		return dtoMovimento;
	}
}
