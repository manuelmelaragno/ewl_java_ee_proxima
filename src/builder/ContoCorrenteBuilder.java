package builder;

import java.util.ArrayList;
import java.util.List;

import dto.ClienteDTO;
import dto.ContoCorrenteDTO;
import dto.MovimentoDTO;
import model.Cliente;
import model.ContoCorrente;
import model.Movimento;

public class ContoCorrenteBuilder {
	public static ContoCorrente convert(ContoCorrenteDTO dtoContoCorrente) {
		ContoCorrente contoCorrente = new ContoCorrente();

		if (dtoContoCorrente.getIdContoCorrente() != null) {
			contoCorrente.setIdContoCorrente(dtoContoCorrente.getIdContoCorrente());
		}
		if (dtoContoCorrente.getDataCreazione() != null) {
			contoCorrente.setDataCreazione(dtoContoCorrente.getDataCreazione());
		}

		if (dtoContoCorrente.getIban() != null && !dtoContoCorrente.getIban().trim().equals("")) {
			contoCorrente.setIban(dtoContoCorrente.getIban());
		}

		if (dtoContoCorrente.getCliente() != null) {
			Cliente cliente = new Cliente();
			cliente.setIdCliente(dtoContoCorrente.getCliente().getIdCliente());
			cliente.setContoCorrente(contoCorrente);

			contoCorrente.setCliente(cliente);
		}

		if (dtoContoCorrente.getSaldo() != null) {
			contoCorrente.setSaldo(dtoContoCorrente.getSaldo());
		}

		if (dtoContoCorrente.getMovimenti() != null && !dtoContoCorrente.getMovimenti().isEmpty()) {
			List<Movimento> movimenti = new ArrayList<Movimento>();
			Movimento movimento = null;
			for (MovimentoDTO dtoMovimento : dtoContoCorrente.getMovimenti()) {
				movimento = new Movimento();
				movimento.setContoCorrente(contoCorrente);
				movimento.setDataMovimento(dtoMovimento.getDataMovimento());
				movimento.setIdMovimento(dtoMovimento.getIdMovimento());
				movimento.setImporto(dtoMovimento.getImporto());
				movimento.setTipoMovimento(dtoMovimento.getTipoMovimento());

				movimenti.add(movimento);
			}

			contoCorrente.setMovimenti(movimenti);
		}

		return contoCorrente;
	}

	public static ContoCorrenteDTO convert(ContoCorrente contoCorrente) {
		ContoCorrenteDTO dtoContoCorrente = new ContoCorrenteDTO();

		if (contoCorrente.getIdContoCorrente() != null) {
			dtoContoCorrente.setIdContoCorrente(contoCorrente.getIdContoCorrente());
		}

		if (contoCorrente.getDataCreazione() != null) {
			dtoContoCorrente.setDataCreazione(contoCorrente.getDataCreazione());
		}

		if (contoCorrente.getIban() != null && !contoCorrente.getIban().trim().equals("")) {
			dtoContoCorrente.setIban(contoCorrente.getIban());
		}

		if (contoCorrente.getCliente() != null) {
			ClienteDTO dtoCliente = new ClienteDTO();
			dtoCliente.setIdCliente(contoCorrente.getCliente().getIdCliente());
			dtoCliente.setContoCorrente(dtoContoCorrente);

			dtoContoCorrente.setCliente(dtoCliente);
		}

		if (contoCorrente.getSaldo() != null) {
			dtoContoCorrente.setSaldo(contoCorrente.getSaldo());
		}

		if (contoCorrente.getMovimenti() != null && !contoCorrente.getMovimenti().isEmpty()) {
			List<MovimentoDTO> dtoMovimenti = new ArrayList<MovimentoDTO>();
			MovimentoDTO dtoMovimento = null;
			for (Movimento movimento : contoCorrente.getMovimenti()) {
				dtoMovimento = new MovimentoDTO();
				dtoMovimento.setContoCorrente(dtoContoCorrente);
				dtoMovimento.setDataMovimento(movimento.getDataMovimento());
				dtoMovimento.setIdMovimento(movimento.getIdMovimento());
				dtoMovimento.setImporto(movimento.getImporto());
				dtoMovimento.setTipoMovimento(movimento.getTipoMovimento());

				dtoMovimenti.add(dtoMovimento);
			}
			dtoContoCorrente.setMovimenti(dtoMovimenti);
		}

		return dtoContoCorrente;
	}

}
