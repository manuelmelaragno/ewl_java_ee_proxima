package builder;

import dto.ClienteDTO;
import model.Cliente;

public class ClienteBuilder {
	public static Cliente convert(ClienteDTO dtoCliente) {
		Cliente cliente = new Cliente();

		if (dtoCliente.getIdCliente() != null) {
			cliente.setIdCliente(dtoCliente.getIdCliente());
		}

		return cliente;
	}

	public static ClienteDTO convert(Cliente cliente) {
		ClienteDTO dtoCliente = new ClienteDTO();

		if (cliente.getIdCliente() != null) {
			dtoCliente.setIdCliente(cliente.getIdCliente());
		}

		return dtoCliente;
	}
}
