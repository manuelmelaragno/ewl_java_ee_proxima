package facade;

import java.sql.SQLException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import builder.ClienteBuilder;
import dao.ClienteDAO;
import dto.ClienteDTO;
import model.Cliente;

/**
 * Session Bean implementation class ClientEJB
 */
@Stateless
@LocalBean
public class ClienteEJB implements ClienteEJBRemote, ClienteEJBLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ClienteEJB() {

	}

	@Override
	public boolean insertCliente() throws ClassNotFoundException, SQLException, NamingException {
		return ClienteDAO.insertCliente(em);
	}

	@Override
	public ClienteDTO getClienteById(Integer idCliente) throws ClassNotFoundException, SQLException, NamingException {
		Cliente cliente = ClienteDAO.getClienteById(idCliente, em);
		return ClienteBuilder.convert(cliente);
	}
}
