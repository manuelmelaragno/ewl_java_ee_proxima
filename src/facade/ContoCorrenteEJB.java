package facade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.iban4j.CountryCode;
import org.iban4j.Iban;

import builder.ContoCorrenteBuilder;
import dao.ClienteDAO;
import dao.ContoCorrenteDAO;
import dto.ContoCorrenteDTO;
import model.Cliente;
import model.ContoCorrente;

/**
 * Session Bean implementation class ContoCorrenteEJB
 */
@Stateless
@LocalBean
public class ContoCorrenteEJB implements ContoCorrenteEJBRemote, ContoCorrenteEJBLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ContoCorrenteEJB() {

	}

	@Override
	public boolean insertContoCorrente(Integer idCliente) throws ClassNotFoundException, SQLException, NamingException {
		Cliente c = null;
		ContoCorrente conto = null;
		Iban iban = Iban.random(CountryCode.IT);

		c = ClienteDAO.getClienteById(idCliente, em);
		conto = ContoCorrenteDAO.getContoByIban(em, iban.toString());

		if (c != null && conto == null) {
			conto = new ContoCorrente();
			conto.setDataCreazione(new Date());
			conto.setIban(iban.toString());
			conto.setSaldo(0);
			conto.setCliente(c);

			return ContoCorrenteDAO.insertContoCorrente(em, conto);
		}

		return false;
	}

	@Override
	public ContoCorrenteDTO getContoById(Integer idConto) throws SQLException, ClassNotFoundException, NamingException {
		ContoCorrente contoCorrente = ContoCorrenteDAO.getContoById(em, idConto);
		return ContoCorrenteBuilder.convert(contoCorrente);
	}

	@Override
	public ContoCorrenteDTO getContoByIdCliente(Integer idCliente) throws SQLException, ClassNotFoundException, NamingException {

		ContoCorrente contoCorrente = ContoCorrenteDAO.getContoByIdCliente(em, idCliente);
		return ContoCorrenteBuilder.convert(contoCorrente);
	}

	@Override
	public List<ContoCorrenteDTO> getContiCorrente() throws ClassNotFoundException, SQLException, NamingException {
		List<ContoCorrenteDTO> dtoConti = new ArrayList<ContoCorrenteDTO>();
		List<ContoCorrente> conti = ContoCorrenteDAO.getContiCorrente(em);

		for (ContoCorrente contoCorrente : conti) {
			ContoCorrenteDTO dtoConto = ContoCorrenteBuilder.convert(contoCorrente);
			dtoConti.add(dtoConto);
		}

		return dtoConti;
	}

	@Override
	public ContoCorrenteDTO getContoByIban(String iban) throws SQLException, ClassNotFoundException, NamingException {
		ContoCorrente contoCorrente = ContoCorrenteDAO.getContoByIban(em, iban);
		return ContoCorrenteBuilder.convert(contoCorrente);
	}

	@Override
	public void deleteContoCorrente(Integer idConto) throws ClassNotFoundException, SQLException, NamingException {
		ContoCorrenteDAO.deleteContoCorrente(em, idConto);
	}

	@Override
	public ContoCorrenteDTO updateContoCorrente(ContoCorrenteDTO dtoConto) {
		ContoCorrente conto = ContoCorrenteBuilder.convert(dtoConto);
		ContoCorrente contoUpdated = ContoCorrenteDAO.updateContoCorrente(em, conto);

		return ContoCorrenteBuilder.convert(contoUpdated);
	}

}
