package facade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import builder.MovimentoBuilder;
import dao.ContoCorrenteDAO;
import dao.MovimentoDAO;
import dto.MovimentoDTO;
import model.ContoCorrente;
import model.Movimento;

/**
 * Session Bean implementation class MovimentoEJB
 */
@Stateless
@LocalBean
public class MovimentoEJB implements MovimentoEJBRemote, MovimentoEJBLocal {

	@PersistenceContext(unitName = "Hib4PU")
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public MovimentoEJB() {
	}

	@Override
	public boolean insertMovimento(Integer idContoCorrente, Integer importo) throws ClassNotFoundException, SQLException, NamingException {
		ContoCorrente conto = ContoCorrenteDAO.getContoById(em, idContoCorrente);
		if (conto != null) {
			Movimento movimento = new Movimento();
			movimento.setImporto(importo);
			movimento.setContoCorrente(conto);
			movimento.setTipoMovimento(importo > 0 ? "PRELIEVO" : "DEPOSITO");
			movimento.setDataMovimento(new Date());

			MovimentoDAO.insertMovimento(em, movimento);
		}
		return false;
	}

	@Override
	public List<MovimentoDTO> getMovimentiByIBAN(String iban) throws ClassNotFoundException, SQLException, NamingException {
		List<MovimentoDTO> dtoMovimenti = new ArrayList<MovimentoDTO>();
		List<Movimento> movimenti = MovimentoDAO.getMovimentiByIBAN(iban, em);

		for (Movimento movimento : movimenti) {
			MovimentoDTO dtoMovimento = MovimentoBuilder.convert(movimento);

			dtoMovimenti.add(dtoMovimento);
		}

		return dtoMovimenti;
	}

	@Override
	public List<MovimentoDTO> getMovimentiByIdConto(Integer idConto) throws ClassNotFoundException, SQLException, NamingException {
		List<MovimentoDTO> dtoMovimenti = new ArrayList<MovimentoDTO>();
		List<Movimento> movimenti = MovimentoDAO.getMovimentiByIdConto(idConto, em);

		for (Movimento movimento : movimenti) {
			MovimentoDTO dtoMovimento = MovimentoBuilder.convert(movimento);

			dtoMovimenti.add(dtoMovimento);
		}

		return dtoMovimenti;
	}

	@Override
	public List<MovimentoDTO> getMovimenti() throws ClassNotFoundException, SQLException, NamingException {
		List<MovimentoDTO> dtoMovimenti = new ArrayList<MovimentoDTO>();
		List<Movimento> movimenti = MovimentoDAO.getMovimenti(em);

		for (Movimento movimento : movimenti) {
			MovimentoDTO dtoMovimento = new MovimentoDTO();

			dtoMovimento = MovimentoBuilder.convert(movimento);
			dtoMovimenti.add(dtoMovimento);
		}

		return dtoMovimenti;
	}

	@Override
	public void deleteMovimento(Integer idMovimento) throws ClassNotFoundException, SQLException, NamingException {
		MovimentoDAO.deleteMovimento(idMovimento, em);
	}

	@Override
	public List<MovimentoDTO> getMovimentiByImporto(Integer importo) {
		List<MovimentoDTO> dtoMovimenti = new ArrayList<MovimentoDTO>();
		List<Movimento> movimenti = MovimentoDAO.getMovimentiByImporto(importo, em);

		for (Movimento movimento : movimenti) {
			MovimentoDTO dtoMovimento = new MovimentoDTO();

			dtoMovimento = MovimentoBuilder.convert(movimento);
			dtoMovimenti.add(dtoMovimento);
		}

		return dtoMovimenti;
	}
}
