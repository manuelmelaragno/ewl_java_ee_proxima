package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Movimento.getAll", query = "SELECT m FROM Movimento m")
public class Movimento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_MOVIMENTO")
	private Integer idMovimento;

	@ManyToOne
	@JoinColumn(name = "ID_CONTO_CORRENTE", nullable = false)
	private ContoCorrente contoCorrente;

	@Column(name = "TIPO_MOVIMENTO")
	private String tipoMovimento;

	@Column(name = "IMPORTO")
	private Integer importo;

	@Column(name = "DATA_MOVIMENTO")
	private Date dataMovimento;

	public Integer getIdMovimento() {
		return idMovimento;
	}

	public void setIdMovimento(Integer idMovimento) {
		this.idMovimento = idMovimento;
	}

	public String getTipoMovimento() {
		return tipoMovimento;
	}

	public ContoCorrente getContoCorrente() {
		return contoCorrente;
	}

	public void setContoCorrente(ContoCorrente contoCorrente) {
		this.contoCorrente = contoCorrente;
	}

	public void setTipoMovimento(String tipoMovimento) {
		this.tipoMovimento = tipoMovimento;
	}

	public Integer getImporto() {
		return importo;
	}

	public void setImporto(Integer importo) {
		this.importo = importo;
	}

	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

}
