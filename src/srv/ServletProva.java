package srv;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.ClienteEJBRemote;
import facade.ContoCorrenteEJBRemote;
import facade.MovimentoEJBRemote;

/**
 * Servlet implementation class ServletProva
 */
@WebServlet("/ServletProva")
public class ServletProva extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// VALORE PRESO DALLA CONSOLE UNA VOLTA CHE IL SERVER E' SALITO, PRESO IL VALORE
	// CON LA PAROLA CHIAVE EXPORTED

	@Resource(mappedName = "java:jboss/exported/Ewl_Java_EE_Proxima/ClienteEJB!facade.ClienteEJBRemote")
	private ClienteEJBRemote clienteEJB;

	@Resource(mappedName = "java:jboss/exported/Ewl_Java_EE_Proxima/MovimentoEJB!facade.MovimentoEJBRemote")
	private MovimentoEJBRemote movimentoEJB;

	@Resource(mappedName = "java:jboss/exported/Ewl_Java_EE_Proxima/ContoCorrenteEJB!facade.ContoCorrenteEJBRemote")
	private ContoCorrenteEJBRemote contoCorrenteEJB;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletProva() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//
//		try {
//			switch (request.getParameter("op")) {
//			case "insertCliente": //
//				clienteEJB.insertCliente();
//				break;
//			case "insertContoCorrente":
//				if (request.getParameter("idCliente") != null && !request.getParameter("idCliente").trim().equals("")) {
//					contoCorrenteEJB.insertContoCorrente(3);
//					response.getWriter().append("Conto corrente inserito");
//					System.out.println("Conto corrente inserito");
//				}
//				break;
//			case "setSaldo":
//				if (request.getParameter("idConto") != null && !request.getParameter("idConto").trim().equals("")
//						&& request.getParameter("importo") != null
//						&& !request.getParameter("importo").trim().equals("")) {
//					contoCorrenteEJB.setSaldo(Integer.parseInt(request.getParameter("importo")),
//							Integer.parseInt(request.getParameter("idConto")));
//					response.getWriter().append("Saldo settato");
//					System.out.println("Saldo settato");
//				}
//				break;
//			case "insertMovimento":
//				if (request.getParameter("idConto") != null && !request.getParameter("idConto").trim().equals("")
//						&& request.getParameter("importo") != null
//						&& !request.getParameter("importo").trim().equals("")) {
//
//					if (movimentoEJB.insertMovimento(Integer.parseInt(request.getParameter("idConto")),
//							Integer.parseInt(request.getParameter("importo")))) {
//						response.getWriter().append("Movimento inserito");
//						System.out.println("Movimento inserito");
//					}
//
//				}
//				break;
//			case "getMovimentiByIban":
//				if (request.getParameter("iban") != null && !request.getParameter("iban").trim().equals("")) {
//					List<Movimento> movimenti = movimentoEJB.getMovimentiByIBAN(request.getParameter("iban"));
//					if (!movimenti.isEmpty()) {
//						int count = 0;
//						for (Movimento m : movimenti) {
//							response.getWriter().append((++count) + "�")
//									.append("\nID movimento : " + m.getIdMovimento())
//									.append("\nImporto : " + m.getImporto() + " E")
//									.append("\nData movimento : " + m.getDataMovimento())
//									.append("\nTipo movimento : " + m.getTipoMovimento() + "\n\n");
//
//						}
//					}
//
//				}
//
//				break;
//			case "getMovimentiByIdConto":
//				List<Movimento> movimenti = movimentoEJB
//						.getMovimentiByIdConto(Integer.parseInt(request.getParameter("idConto")));
//				if (!movimenti.isEmpty()) {
//					int count = 0;
//					for (Movimento m : movimenti) {
//						response.getWriter().append((++count) + "�").append("\nID movimento : " + m.getIdMovimento())
//								.append("\nImporto : " + m.getImporto() + " E")
//								.append("\nData movimento : " + m.getDataMovimento())
//								.append("\nTipo movimento : " + m.getTipoMovimento() + "\n\n");
//					}
//				}
//				break;
//			default:
//				break;
//			}
//		} catch (ClassNotFoundException | SQLException | NamingException e) {
//			e.printStackTrace();
//		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
