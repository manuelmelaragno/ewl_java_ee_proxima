package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import model.ContoCorrente;

public class ContoCorrenteDAO {
//	private static Connection connectToDatabase() throws ClassNotFoundException, SQLException, NamingException {
//
////		String driver = "com.mysql.jdbc.Driver";
////
////		Class.forName(driver);
////
////		// Creiamo la stringa di connessione
////		String url = "jdbc:mysql://localhost:3306/gestionale_melaragno?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
////
////		// Otteniamo una connessione con username e password
////		return DriverManager.getConnection(url, "root", "1234");
//
//		DataSource ds = (DataSource) new InitialContext().lookup("java:jboss/datasources/MySqlDS_Ewl");
//		return ds.getConnection();
//	}

	public static boolean insertContoCorrente(EntityManager em, ContoCorrente conto) {

		em.persist(conto);

		return true;
	}

	public static void deleteContoCorrente(EntityManager em, Integer idConto) {
		ContoCorrente conto = em.find(ContoCorrente.class, idConto);
		if (conto != null)
			em.remove(conto);
	}

	public static ContoCorrente updateContoCorrente(EntityManager em, ContoCorrente conto) {
		ContoCorrente contoUpdate = null;

		ContoCorrente old = ContoCorrenteDAO.getContoById(em, conto.getIdContoCorrente());
		if (conto.getDataCreazione() == null)
			conto.setDataCreazione(old.getDataCreazione());
		if (conto.getSaldo() == null)
			conto.setSaldo(old.getSaldo());
		if (conto.getCliente() == null)
			conto.setCliente(old.getCliente());
		if (conto.getIban() == null)
			conto.setIban(old.getIban());
		contoUpdate = em.merge(conto);

		return contoUpdate;
	}

	public static ContoCorrente getContoById(EntityManager em, Integer idConto) {
		ContoCorrente conto = null;

		conto = em.find(ContoCorrente.class, idConto);

		return conto;
	}

	public static ContoCorrente getContoByIdCliente(EntityManager em, Integer idCliente) {
		ContoCorrente conto = null;

		Query q = em.createQuery("SELECT c FROM ContoCorrente c WHERE c.cliente.idCliente = :idCliente");
		q.setParameter("idCliente", idCliente);

		try {
			conto = (ContoCorrente) q.getSingleResult();
		} catch (NoResultException e) {
			conto = null;
		}

		return conto;
	}

	public static ContoCorrente getContoByIban(EntityManager em, String iban) {
		ContoCorrente conto = null;

		Query q = em.createQuery("SELECT c FROM ContoCorrente c WHERE c.iban = :iban");
		q.setParameter("iban", iban);

		try {
			conto = (ContoCorrente) q.getSingleResult();
		} catch (NoResultException e) {
			conto = null;
		}

		return conto;
	}

	@SuppressWarnings("unchecked")
	public static List<ContoCorrente> getContiCorrente(EntityManager em) {
		List<ContoCorrente> conti = new ArrayList<ContoCorrente>();

		conti = em.createNamedQuery("ContoCorrente.getAll").getResultList();

		return conti;
	}

}
