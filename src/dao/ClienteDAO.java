package dao;

import java.sql.SQLException;

import javax.naming.NamingException;
import javax.persistence.EntityManager;

import model.Cliente;

public class ClienteDAO {

	public static boolean insertCliente(EntityManager em) throws ClassNotFoundException, SQLException, NamingException {
		boolean ret = true;

		Cliente cliente = new Cliente();
		em.persist(cliente);

		return ret;
	}

	public static Cliente getClienteById(Integer idCliente, EntityManager em) throws ClassNotFoundException, SQLException, NamingException {
		Cliente c = null;

		c = em.find(Cliente.class, idCliente);

		return c;
	}
}
