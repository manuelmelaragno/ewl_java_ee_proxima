package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Movimento;

public class MovimentoDAO {

	public static boolean insertMovimento(EntityManager em, Movimento movimento) {
		boolean ret = false;

		return ret;
	}

	public static void deleteMovimento(Integer idMovimento, EntityManager em) {

		Movimento mov = em.find(Movimento.class, idMovimento);

		if (mov != null)
			em.remove(mov);

	}

	@SuppressWarnings("unchecked")
	public static List<Movimento> getMovimenti(EntityManager em) {
		List<Movimento> movimenti = new ArrayList<Movimento>();

		movimenti = em.createNamedQuery("Movimento.getAll").getResultList();

		return movimenti;
	}

	@SuppressWarnings("unchecked")
	public static List<Movimento> getMovimentiByIdConto(Integer idConto, EntityManager em) {
		List<Movimento> movimenti = new ArrayList<Movimento>();

		Query q = em.createQuery("SELECT m FROM Movimento m WHERE m.contoCorrente.idContoCorrente = :idConto");

		q.setParameter("idConto", idConto);

		movimenti = q.getResultList();

		return movimenti;
	}

	@SuppressWarnings("unchecked")
	public static List<Movimento> getMovimentiByIBAN(String iban, EntityManager em) {
		List<Movimento> movimenti = new ArrayList<Movimento>();

		Query q = em.createQuery("SELECT m FROM Movimento m WHERE m.contoCorrente.iban = :iban");

		q.setParameter("iban", iban);

		movimenti = q.getResultList();

		return movimenti;
	}

	@SuppressWarnings("unchecked")
	public static List<Movimento> getMovimentiByImporto(Integer importo, EntityManager em) {
		List<Movimento> movimenti = new ArrayList<Movimento>();

		Query q = em.createQuery("SELECT m FROM Movimento m WHERE m.importo = :importo");

		q.setParameter("importo", importo);

		movimenti = q.getResultList();

		return movimenti;
	}
}
